describe 'sales_tax_calculator' do
  (1..3).each do |n|
    it "should append correct sales tax to input#{n}.txt" do
      input_file = File.dirname(__FILE__) + "/../files/input#{n}.txt"
      output_file = File.dirname(__FILE__) + "/../files/output#{n}.txt"

      expected = File.read(output_file)
      expect(`sales_tax_calculator < #{input_file}`).to eq(expected)
    end
  end
end
