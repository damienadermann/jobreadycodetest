require 'spec_helper'
require 'bigdecimal'

module SalesTaxCalculator
  describe Product do
    it 'has correct attributes' do
      product = Product.new 1, 'book', BigDecimal.new('12.49')
      expect(product.quantity).to be(1)
      expect(product.name).to match('book')
      expect(product.original_price).to eq BigDecimal.new('12.49')
    end

    describe '#price' do
      it 'has original price if no sales tax or import tax' do
        product = Product.new(1, 'music cd', BigDecimal.new('14.99'))
        expect(product.price).to eq BigDecimal.new('14.99')
      end

      it 'has correct price if has sales tax' do
        product = Product.new(1, 'music cd', BigDecimal.new('14.99'), true)
        expect(product.price).to eq BigDecimal.new('16.49')
      end

      it 'has correct price if has sales import duty' do
        product = Product.new(1, 'imported something', BigDecimal.new('10.00'), false, true)
        expect(product.price).to eq BigDecimal.new('10.50')
      end

      it 'has correct price if has product has sales tax and import duty' do
        product = Product.new(1, 'imported something', BigDecimal.new('47.50'), true, true)
        expect(product.price).to eq BigDecimal.new('54.63')
      end
    end

    describe '#basic_sales_tax' do
      it 'gets just the sales tax portion of the price' do
        product = Product.new(1, 'music cd', BigDecimal.new('14.99'), true)
        expect(product.basic_sales_tax).to eq BigDecimal.new('1.50')

        product = Product.new(1, 'music cd', BigDecimal.new('14.99'), false)
        expect(product.basic_sales_tax).to eq BigDecimal.new('0.00')
      end
    end

    describe '#import_duty' do
      it 'gets just the import dudy portion of the price' do
        product = Product.new(1, 'music cd', BigDecimal.new('14.99'), true, true)
        expect(product.import_duty).to eq BigDecimal.new('0.75')

        product = Product.new(1, 'music cd', BigDecimal.new('14.99'), false, false)
        expect(product.import_duty).to eq BigDecimal.new('0.00')
      end
    end
  end
end
