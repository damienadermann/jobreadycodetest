require 'spec_helper'

module SalesTaxCalculator
  describe Order do
    let(:order) { Order.new }

    describe '#add_product' do
      it 'can add a product' do
        product = double(Product)

        order.add_product product
        expect(order.products.pop).to be product
      end
    end

    describe '#sales_tax' do
      it 'is handles no products' do
        expect(order.sales_tax).to eq BigDecimal.new("0.00")
      end

      it 'is sum of products sales_tax rounded to nearest .05' do
        product1 = double(Product, total_sales_tax: 1.11)
        product2 = double(Product, total_sales_tax: 2.2)

        order.add_product product1
        order.add_product product2

        expect(order.sales_tax).to eq BigDecimal.new("3.30")
      end
    end

    describe '#total_price' do
      it 'is handles no products' do
        expect(order.total_price).to eq BigDecimal.new("0.00")
      end

      it 'is sum of products price' do
        product1 = double(Product, price: 1.11)
        product2 = double(Product, price: 2.2)

        order.add_product product1
        order.add_product product2

        expect(order.total_price).to eq BigDecimal("3.31")
      end
    end

  end
end
