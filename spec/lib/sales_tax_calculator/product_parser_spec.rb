require 'spec_helper'
require 'bigdecimal'

module SalesTaxCalculator
  describe ProductParser do
    let(:product_parser) { ProductParser.new }

    describe '#parse_product' do
      it 'is false on incorrect product' do
        product_string = ''
        product = product_parser.parse_product(product_string)

        expect(product).to be(false)
      end

      it 'can parse a correct product' do
        product_string = '1, book, 12.49'
        product = product_parser.parse_product(product_string)

        expect(product.quantity).to be(1)
        expect(product.name).to match(/book/)
        expect(product.original_price).to eq BigDecimal.new('12.49')
      end

      it 'sets a regular product to have a sales tax' do
        product_string = '1, cheese, 12.49'
        product = product_parser.parse_product(product_string)

        expect(product.has_sales_tax).to be true
      end

      ['book', 'chocolate bar', 'imported box of chocolates', 'packet of headache pills'].each do |product_name|
        it 'assigns sales tax correctly' do
          product_string = "1, #{product_name}, 12.49"
          product = product_parser.parse_product(product_string)

          expect(product.has_sales_tax).to be false
        end
      end

      it 'sets import duty only on imported products' do
        product_string = '1, cheese, 12.49'
        product = product_parser.parse_product(product_string)

        expect(product.has_import_duty).to be false

        product_string = '1, imported cheese, 12.49'
        product = product_parser.parse_product(product_string)

        expect(product.has_import_duty).to be true
      end
    end

  end
end
