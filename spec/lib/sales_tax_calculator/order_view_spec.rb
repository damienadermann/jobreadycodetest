require 'spec_helper'

module SalesTaxCalculator

  describe OrderView do
    describe '#render' do
      it 'renders empty order' do
        order = Order.new
        order_view = OrderView.new(order)

        expected = %{
Sales Taxes: 0.00
Total: 0.00}

        expect(order_view.render).to match(expected)
      end

      it 'renders product' do
        product1 = Product.new(1, 'book', 2.00, true)
        product2 = Product.new(1, 'Chook', 3.00)
        order = Order.new
        order.add_product(product1)
        order.add_product(product2)
        order_view = OrderView.new(order)

        expected = %{1, book, 2.20
1, Chook, 3.00

Sales Taxes: 0.20
Total: 5.20}

        expect(order_view.render).to match(expected)
      end
    end
  end
end
