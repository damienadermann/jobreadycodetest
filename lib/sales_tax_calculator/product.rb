require 'bigdecimal'

module SalesTaxCalculator
  class Product
    attr_accessor :quantity
    attr_accessor :name
    attr_accessor :original_price
    attr_accessor :has_sales_tax
    attr_accessor :has_import_duty

    SALES_TAX_RATE = BigDecimal.new("0.1")
    IMPORT_DUTY = BigDecimal.new("0.05")

    def initialize(quantity, name, original_price, has_sales_tax = false, has_import_duty = false)
      @quantity = quantity
      @name = name
      @original_price = original_price
      @has_sales_tax = has_sales_tax
      @has_import_duty = has_import_duty
    end

    def price
      (original_price + total_sales_tax).round(2)
    end

    def total_sales_tax
      basic_sales_tax + import_duty
    end

    def basic_sales_tax
      if @has_sales_tax == true
        (original_price * SALES_TAX_RATE).round(2)
      else
        BigDecimal.new("0.00")
      end
    end

    def import_duty
      if @has_import_duty == true
        (original_price * IMPORT_DUTY).round(2)
      else
        BigDecimal.new("0.00")
      end
    end
  end
end
