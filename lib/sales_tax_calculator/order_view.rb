module SalesTaxCalculator
  class OrderView
    def initialize(order)
      @order = order
    end

    def render
      lines = []
      @order.products.each do |product|
        lines << render_product(product)
      end
      lines << totals_string
      lines.join("\n")
    end

    private
      def render_product(product)
        "#{product.quantity}, #{product.name}, #{sprintf( "%0.02f", product.price)}"
      end

      def totals_string
        [
          '',
          "Sales Taxes: #{sprintf( "%0.02f", @order.sales_tax)}",
          "Total: #{sprintf( "%0.02f", @order.total_price)}"
        ].join("\n")
      end
  end
end
