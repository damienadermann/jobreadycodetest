module SalesTaxCalculator
  class Order
    attr_reader :products

    def initialize
      @products = []
    end

    def add_product(product)
      @products << product
    end

    def sales_tax
      if @products.size != 0
        round(@products.map(&:total_sales_tax).reduce(:+))
      else
        0.0
      end
    end

    def total_price
      if @products.size != 0
        @products.map(&:price).reduce(:+).round(2)
      else
        BigDecimal.new('0')
      end
    end

    private
      def round(amount)
        (amount*2).round(1) / BigDecimal("2.0")
      end

  end
end
