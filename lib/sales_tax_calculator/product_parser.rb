require 'bigdecimal'

module SalesTaxCalculator
  class ProductParser
    def parse_product(line)
      raw_attributes = line.split(',').map(&:strip)
      return false if raw_attributes.size != 3

      quantity, name, price = raw_attributes
      quantity = quantity.to_i
      price = BigDecimal.new(price)

      Product.new(quantity, name, price, product_has_sales_tax?(name), product_has_import_duty?(name))
    end

    private
      def product_has_sales_tax?(product_name)
        (product_name =~ /(book)|(chocolate)|(pill)/).nil?
      end

      def product_has_import_duty?(product_name)
        ! (product_name =~ /imported/).nil?
      end
  end
end
