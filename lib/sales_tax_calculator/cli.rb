module SalesTaxCalculator

  class CLI
    attr_accessor :reader
    attr_accessor :output
    attr_accessor :productParser

    def initialize()
      @productParser = ProductParser.new
      @reader = $stdin
      @output = $stdout
      order = read_input
      show_order(order)
    end

    private
      def read_input
        order = Order.new
        reader.gets #discard first line
        while product = read_product_from_reader
          order.add_product(product)
        end
        order
      end

      def read_product_from_reader()
        line = reader.gets.chomp
        productParser.parse_product(line)
      end

      def show_order(order)
        order_view = OrderView.new(order)
        output.puts order_view.render
      end
  end
end
